$(document).ready(function() {
  $("#register_form").on('submit', function(e) {
    e.preventDefault()
    $.ajax({
      url: '/register/form/',
      type: 'POST',
      data: {email: $('#email_message').val(),
             namefield: $('#name_message').val(),
             password: $('#pw_message').val(),
             csrfmiddlewaretoken: $('input[name = csrfmiddlewaretoken]').val()},

      success: function(response) {
        alert('Success!');
        window.location.href = "/";
      }
    })
  });
})

$(document).on('keyup', '#email_message', function(e) {
  e.preventDefault();
  console.log('email validation')
  $.ajax({
    url: '/register/form/?email=' + $("#email_message").val(),
    success: function(response) {
      if (response.is_taken) {
        console.log('Email already taken')
        alert('E-mail is already taken. Please enter unique e-mail')
        $('#email_message').val("");
      }
    }
  })
})
