var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}

function changeTheme() {
  document.getElementsByTagName("body").style.backgroundColor = 'black';
  document.getElementsByClassName("container-fluid").style.backgroundColor = 'black';
  document.getElementsByClassName("topnav").style.backgroundColor = 'black';
  document.getElementsByClassName("active").style.backgroundColor = 'black';
  document.getElementsByClassName("accordion").style.backgroundColor = 'black';
  document.getElementsByClassName("panel").style.backgroundColor = 'black';

  document.getElementsByClassName("topnav").style.color = '#8fbcbc';
  document.getElementsByClassName("active").style.color = '#8fbcbc';
  document.getElementsByClassName("accordion").style.color = '#8fbcbc';



}
