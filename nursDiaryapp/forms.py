from django import forms
from django.forms import ModelForm
from .models import *
import random


class DiaryForm(forms.ModelForm):
    placeholder_messages = ["What's goin on today?",
                            "Do you have anything special today?",
                            "Tell us something about your day!",
                            "What's on your mind?",
                            "Dear diary..." ]
    random_placeholder = placeholder_messages[random.randint(0, len(placeholder_messages)-1)]
    attrs = { 'class': 'form-control',
              'type': 'text',
              'placeholder': random_placeholder}

    diary = forms.CharField(label='', required=True,  widget=forms.Textarea(attrs=attrs))

    class Meta:
        model = Diary
        fields = ['diary']

class RegisterForm(forms.ModelForm):
    email_attrs = {
        'class': 'form-control',
        'placeholder': 'Enter your unique e-mail address',
        'id': 'email_message'
    }
    name_attrs = {
        'class': 'form-control',
        'placeholder': 'Enter your name',
        'id': 'name_message'
    }
    password_attrs = {
        'class': 'form-control',
        'placeholder': 'Enter a valid password',
        'id': 'pw_message'
    }

    email = forms.EmailField(label="E-mail", required=True, widget=forms.EmailInput(attrs=email_attrs))
    namefield = forms.CharField(label="Full Name", required=True, max_length=40, widget=forms.TextInput(attrs=name_attrs))
    password = forms.CharField(label="Password", required=True, max_length=40, widget=forms.PasswordInput(attrs=password_attrs))

    class Meta:
        model = Register
        fields = '__all__'
