from django.test import TestCase, Client
from django.urls import resolve, reverse

from .views import diary, profile
from .models import Diary
from .forms import DiaryForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class FunctionalTestLandingPage(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(FunctionalTestLandingPage, self).setUp()

    def test_landingpage_contains_hidden_loader(self):
        selenium = self.selenium
        # Opening the landing page link
        selenium.get('http://tempatcurahanhati.herokuapp.com/diary/')
        time.sleep(3)

        # Find the loader css display property
        hidden_loader = selenium.find_element_by_id('loader').value_of_css_property('display')

        self.assertIn('none', hidden_loader)

    def test_can_post_valid_message(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://tempatcurahanhati.herokuapp.com/diary/')
        time.sleep(3)

        # find the elements
        diary_message = selenium.find_element_by_id('id_diary')
        submit_button = selenium.find_element_by_class_name('btn-default')
        posted_message = selenium.find_element_by_class_name('card-title')

        # Fill the form with data
        diary_message.send_keys('Message posted correctly')

        # Sumbit message
        submit_button.send_keys(Keys.RETURN)

        # Message posted
        self.assertIn('Message posted correctly', selenium.page_source)

    def test_cant_post_invalid_message(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/diary/')
        # find the elements
        diary_message = selenium.find_element_by_id('id_diary')
        submit_button = selenium.find_element_by_class_name('btn-default')
        posted_message = selenium.find_element_by_class_name('card-title')

        # Fill the form with data
        morethan300_message = 'invalid'*45
        diary_message.send_keys(morethan300_message)

        # Sumbit message
        submit_button.send_keys(Keys.RETURN)

        # Message posted
        self.assertIn('Ensure this value has at most 300 characters', selenium.page_source)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestLandingPage, self).tearDown()

    def test_landingpage_contains_correct_h3_title(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/diary/')
        time.sleep(3)

        # find the h3 elements
        h3_title = selenium.find_element_by_tag_name('h3').text

        self.assertIn('Your Brand New Diaries..', h3_title)

    def test_landingpage_contains_correct_h5_greetings(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/diary/')
        time.sleep(3)
        # find the h5 elements
        h5_greetings = selenium.find_element_by_tag_name('h5').text

        self.assertIn('Hallo, apa kabar?', h5_greetings)

    def test_titlefont_in_correct_color(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/diary/')
        # find the title class and its css color property
        titlefont_color = selenium.find_element_by_class_name('title-page').value_of_css_property('color')

        self.assertIn('rgba(188, 143, 143, 1)', titlefont_color)

    def test_titlefont_in_correct_bg_color(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/diary/')
        # find the title class and its css background-color property
        titlefont_bg_color = selenium.find_element_by_class_name('title-page').value_of_css_property('background-color')
        self.assertIn('rgba(255, 192, 203, 1)', titlefont_bg_color)

class FunctionalTestProfilePage(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(FunctionalTestProfilePage, self).setUp()

    def test_profilepage_contains_hidden_loader(self):
        selenium = self.selenium
        # Opening the profile page link
        selenium.get('http://tempatcurahanhati.herokuapp.com/')
        time.sleep(3)

        # find the loader css display property
        hidden_loader = selenium.find_element_by_id('loader').value_of_css_property('display')

        self.assertIn('none', hidden_loader)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestProfilePage, self).tearDown()

    def test_page_contains_correct_image(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/')

        # find image element
        profile_image = selenium.find_element_by_xpath("//img[@src='https://scontent.fcgk4-2.fna.fbcdn.net/v/t1.0-9/56759172_2803107366382464_5543785975693443072_n.jpg?_nc_cat=106&_nc_ht=scontent.fcgk4-2.fna&oh=75dbfa26053490e54e6d999633e9b50d&oe=5D3F51E7']")
        # get image src attribute
        profile_image = profile_image.get_attribute('src')

        self.assertIn('https://scontent.fcgk4-2.fna.fbcdn.net/v/t1.0-9/56759172_2803107366382464_5543785975693443072_n.jpg?_nc_cat=106&_nc_ht=scontent.fcgk4-2.fna&oh=75dbfa26053490e54e6d999633e9b50d&oe=5D3F51E7', profile_image)

    def test_page_contains_my_portofolio(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/')
        time.sleep(3)

        # find string elements
        str_name = selenium.find_element_by_class_name('my-name').text
        str_obj = selenium.find_element_by_class_name('string').text

        self.assertIn('NUR NISRINA NINGRUM', str_name)
        self.assertIn('A Computer Science student at University of Indonesia', str_obj)
        self.assertIn('+6289601438752', str_obj)

    def test_profile_contains_accordion(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/')
        time.sleep(3)

        # find accordion
        exp = selenium.find_element_by_id('exp').text

        self.assertIn('EXPERIENCE', exp)

    def test_profilpage_contains_changetheme_button(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/')
        time.sleep(3)

        #find button
        changetheme_bttn = selenium.find_element_by_class_name('change-theme').text

        self.assertIn('Poof!', changetheme_bttn)

    def test_profilpage_contains_subscribe_button(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/')
        time.sleep(3)

        #find button
        subs_btn = selenium.find_element_by_id('subs-btn').text

        self.assertIn('Subscribe', subs_btn)

class FunctionalTestBookPage(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(FunctionalTestBookPage, self).setUp()

    def test_bookpage_contains_hidden_loader(self):
        selenium = self.selenium
        # Opening the book page link
        selenium.get('http://tempatcurahanhati.herokuapp.com/books/')
        time.sleep(3)

        # find the loader css display property
        hidden_loader = selenium.find_element_by_id('loader').value_of_css_property('display')

        self.assertIn('none', hidden_loader)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestBookPage, self).tearDown()

    def test_page_contains_correct_image(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/books/')

        # find image element
        pstar_image = selenium.find_element_by_xpath("//img[@src='https://i0.wp.com/whateverbrightthings.com/wp-content/uploads/2016/12/Pink-Star-Folder.png?fit=1400%2C1400']")
        # get image src attribute
        pstar_image = pstar_image.get_attribute('src')

        self.assertIn('https://i0.wp.com/whateverbrightthings.com/wp-content/uploads/2016/12/Pink-Star-Folder.png?fit=1400%2C1400', pstar_image)

    def test_bookpage_contains_star_image_in_table(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/books/')

        # find and get its table element
        ystar_image = selenium.find_element_by_xpath("//img[@src='https://img.icons8.com/carbon-copy/2x/star.png']")
        # get image src attribute
        ystar_image = ystar_image.get_attribute('src')

        self.assertIn("https://img.icons8.com/carbon-copy/2x/star.png", ystar_image)

class FunctionalTestRegisterPage(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(FunctionalTestRegisterPage, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestRegisterPage, self).tearDown()

    def test_registerpage_contains_form_title(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/register/')
        time.sleep(3)

        # find title
        title_page = selenium.find_element_by_class_name('title-page').text

        self.assertIn('Registration Form', title_page)

    def test_registerpage_contains_signup_button(self):
        selenium = self.selenium
        selenium.get('http://tempatcurahanhati.herokuapp.com/register/')
        time.sleep(3)

        #find button
        signup_btn = selenium.find_element_by_id('signup-btn').text

        self.assertIn('Sign me up!', signup_btn)

class DiaryPageTest(TestCase):

    def test_diary_url_exists(self):
        response = Client().get('/diary/')
        self.assertEqual(response.status_code, 200)

    def test_diary_using_landingpagehtml(self):
        response = Client().get('/diary/')
        self.assertTemplateUsed(response, 'landingpage.html')

    def test_diary_using_diary_func(self):
        found = resolve('/diary/')
        self.assertEqual(found.func, diary)

    def test_diary_view_no_status_render_correctly(self):
        empty_form = DiaryForm()
        response = Client().get('/diary/')
        self.assertContains(response, 'Hallo, apa kabar?')

class ProfilePageTest(TestCase):

    def test_profile_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_profile_using_profilepagehtml(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'profilepage.html')

    def test_profile_using_profile_func(self):
        found = resolve('/')
        self.assertEqual(found.func, profile)

    def test_profile_has_correct_content(self):
        response = Client().get('/')
        self.assertContains(response, 'NUR NISRINA NINGRUM')
        self.assertContains(response, 'A Computer Science student at University of Indonesia')
        self.assertContains(response, '+6289601438752')

class BookPageTest(TestCase):

    def test_book_url_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_bookpage_using_bookpagehtml(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'bookpage.html')

class RegisterPageTest(TestCase):

    def test_register_url_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_registerpage_using_registerpagehtml(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'registerpage.html')
