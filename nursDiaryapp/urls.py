from django.urls import re_path
from .views import *
#url for app
urlpatterns = [
    re_path(r'^$', profile, name='profile'),
    re_path(r'^diary/$', diary, name='diary'),
    re_path(r'^diary/delete/$', delete, name='delete'),
    re_path(r'^profile/$', profile, name='profile'),
    re_path(r'^books/$', books, name='books'),
    re_path(r'^books/(?P<judul>[^/]+)/$', get_books, name='get_books'),
    re_path(r'^register/$', register, name='register'),
    re_path(r'^register/form/', register_form, name='register_form')
]
